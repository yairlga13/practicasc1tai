function buscar() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users";

    // validar la respuesta
    http.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {
            // aqui se dibuja la pagina
            let res = document.getElementById("tabla");
            const json = JSON.parse(this.responseText);

            // CICLO PARA IR TOMANDO CADA UNO DE LOS REGISTROS
            var elementoHTML = document.getElementById("inputBuscar");

            res.innerHTML = ""; // Limpiar resultados previos

            if (elementoHTML.value.trim() === "") {
                res.innerHTML += '<tr> <td style="display: flex; align-items: center; justify-content: center; COLOR:RED; font-size: 30px;" class="columna3">ESCRIBE UN ID</td> </tr>';
                return; // Salir de la función si no se ha escrito ningún ID
            }

            let idEncontrado = false;

            for (const datos of json) {
                if (datos.id == elementoHTML.value) {
                    res.innerHTML += '<tr> <td class="columna">' +'ID:'+ datos.id+'<br>' +
                    'Name:'+ datos.name+'<br>' +'Email:'+ datos.email+'<br>' +'ADDRESS: <br>'+
                    'STREES:'+ datos.address.street+'<br>'+'SUITE:'+ datos.address.suite+'<br>'+
                     'CITY:'+ datos.address.city+'<br>'+'ZIPCODE:'+ datos.address.zipcode+'<br>'+
                     'GEO: <br>'+'LAT:'+ datos.address.geo.lat+'<br>'+'LNG:'+ datos.address.geo.lng+
                     '<br>'+'PHONE:'+ datos.phone+'<br>'+'WEBSITE:'+ datos.website+'<br>'+'COMPANY: <br>'+
                     'NAME:'+ datos.company.name+'<br>'+'CATCHPRASE:'+ datos.company.catchPhrase+'<br>'+ 
                     'BS:'+ datos.company.bs+'<br>'+ 
                     '</td> </tr>';
                    idEncontrado = true;
                    break; // Romper el bucle una vez que se encuentra el ID
                }
            }

            if (!idEncontrado) {
                if (isNaN(elementoHTML.value)) {
                    res.innerHTML += '<tr> <td style="display: flex; align-items: center; justify-content: center; COLOR:RED; font-size: 30px;" class="columna3">ID NO NUMÉRICO, ESCRIBE UN CARÁCTER NUMÉRICO</td> </tr>';
                } else {
                    res.innerHTML += '<tr> <td style="display: flex; align-items: center; justify-content: center; COLOR:RED; font-size: 30px;" class="columna3">ID NO EXISTENTE</td> </tr>';
                }
            }
        }
    };

    http.open('GET', url, true);
    http.send();
}

// codificar los botones
document.getElementById("btnBuscar").addEventListener("click", function () {
    buscar();
});

document.getElementById("btnLimpiar").addEventListener("click", function () {
    document.getElementById("tabla").innerHTML = "";
    document.getElementById("inputBuscar").value = ""; 
});