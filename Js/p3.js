function hacerPeticion() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users";

    http.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {
            let res = document.getElementById("lista");
            const json = JSON.parse(this.responseText);

            for (const datos of json) {
                res.innerHTML += '<tr>' +
                    '<td class="columna1">' + datos.id + '</td>' +
                    '<td class="columna2">' + datos.name + '</td>' +
                    '<td class="columna3">' + datos.username + '</td>' +
                    '<td class="columna4">' + datos.email + '</td>' +
                    '<td class="columna5">' + datos.address.street+','+ datos.address.suite+','+ datos.address.city+','+ datos.address.zipcode+','+ datos.address.geo.lat+','+ datos.address.geo.lng+ '</td>' +
                    '<td class="columna6">' + datos.phone + '</td>' +
                    '<td class="columna7">' + datos.website + '</td>' +
                    '<td class="columna8">' + datos.company.name+','+datos.company.catchPhrase+','+datos.company.bs + '</td>' +
                    '</td>' +
                    '</tr>';
            }
        }
    };
    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnCargar").addEventListener("click", function () {
    hacerPeticion();
});

document.getElementById("btnLimpiar").addEventListener("click", function () {
    document.getElementById("lista").innerHTML = "";
});
